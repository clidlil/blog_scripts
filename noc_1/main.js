var caminante;
var tx = 0.5;
var ty = 1.5;

function setup()
{ 
   createCanvas(400, 400);
   // caminante = new CaminantePeril(width/2, height/2);
   caminante = new Caminante(width/2, height/2);
}

function draw()
{
   background(200);
   caminante.mostrar();
   caminante.caminar();
   tx += 0.01;
   ty += 0.01;
}
