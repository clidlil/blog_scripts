class Caminante
{
   constructor(x, y)
   {
      this.x = x;
      this.y = y;
   }

   mostrar()
   {
      stroke(50);
      fill(204, 51, 10);
      ellipse(this.x, this.y, 10, 10);
   }

   caminar()
   {
      let choice = int(random(4));
      if (choice === 0)
         this.x += 5;
      else if (choice === 1)
         this.x -= 5
      else if (choice === 2)
         this.y += 5;
      else
         this.y -= 5;
   }
}

class CaminantePeril extends Caminante
{
   constructor(x, y)
   {
      super(x, y);
   }

   caminar(t)
   {
      let nx = noise(tx);
      let ny = noise(ty);
      this.x = map(nx, 0, 1, 0, width);
      this.y = map(ny, 0, 1, 0, height);
   }
}
