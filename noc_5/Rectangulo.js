class Rectangulo
{
    constructor(posicion, color, masa)
    {
        this.posicion = posicion;
        this.velocidad = createVector(0, 0);
        this.velocidadMaxima = 8;
        this.aceleracion = createVector(0, 0);
        this.masa = masa;

        this.color = color;
        this.diametro = masa;
    }

    aplicarFuerza(f)
    {
        let fuerza = f.copy();
        fuerza.div(this.masa);
        this.aceleracion.add(fuerza);
    }

    mover()
    {
        this.velocidad.add(this.aceleracion);
        this.velocidad.limit(this.velocidadMaxima);
        this.posicion.add(this.velocidad);
        this.aceleracion.mult(0);
    }

    verificarBorde()
    {
        let radio = this.diametro/2;
        if (this.posicion.x > width)
        {
            this.posicion.x = 0;
            // this.velocidad.x *= -1;
        }
        if (this.posicion.x < 0)
        {
            this.posicion.x = width;
            // this.velocidad.x *= -1;
        }

        if (this.posicion.y > height)
        {
            this.posicion.y = 0;
            // this.velocidad.y *= -1;
        }
        if (this.posicion.y < 0)
        {
            this.posicion.y = width;
            //this.velocidad.y *= -1;
        }
    }

    mostrar()
    {
        let angulo = atan(this.velocidad.y/this.velocidad.x);
        stroke(35);
        fill(this.color);
        push();
        rectMode(CENTER);
        translate(this.posicion.x, this.posicion.y);
        rotate(angulo);
        rect(0, 0, 2*this.diametro, this.diametro);
        pop();
    }
}
