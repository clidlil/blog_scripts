let pendulo;

function setup()
{
    createCanvas(640, 360);
    let origen = createVector(width/2, 10);
    let radio = 280;
    let gravedad = 0.5;

    pendulo = new Pendulo(origen, radio, gravedad);
}

function draw()
{
    background(245);
    pendulo.actualizar();
    pendulo.mostrar();
}
