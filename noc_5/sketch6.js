let resorte;
let gravedad;
let pelota;
let pelota2;

function setup()
{
    createCanvas(600, 600)
    background(220);
    let color = [0, 50, 240];
    let rigidez = 0.01;
    let ancla = createVector(width/2, 50);
    let extremo = createVector(width - 100, 200);
    resorte = new Resorte(ancla, extremo, rigidez, color);
    pelota = new Pelota(resorte.extremo, color, 50);
    pelota2 = new Pelota(extremo.copy(), color, 50);
    
    gravedad = createVector(0, 0.5);
    
}

function draw()
{
    background(220);

    pelota2.aplicarFuerza(gravedad);
    pelota2.mover();
    pelota2.verificarBorde();

    let fuerza = p5.Vector.mult(gravedad, 0.1*pelota.masa);
    resorte.deformar(fuerza);
    pelota.verificarBorde();

    pelota.mostrar();
    pelota2.mostrar();
    resorte.mostrar();
}
