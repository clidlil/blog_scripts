class Resorte
{
    constructor(ancla, extremo, rigidez, color)
    {
        this.ancla = ancla;
        this.extremo = extremo;
        let hilo = p5.Vector.sub(this.extremo, this.ancla);
        this.longitud = hilo.mag();
        this.rigidez = rigidez;
        this.color = color;
        this.velocidadExtremo = createVector(0, 0);
    }

    deformar(f)
    {
        let fuerza = f.copy();
        let magnitud = fuerza.mag();
        let deformacion = magnitud - this.longitud;
        
        let coeficienteFriccion = 0.02;

        fuerza.normalize();
        fuerza.mult(-1 * this.rigidez * deformacion);
        
        this.velocidadExtremo.add(fuerza);

        let friccion = this.velocidadExtremo.copy();
        friccion.normalize();
        friccion.mult(-1*coeficienteFriccion);
        this.velocidadExtremo.add(friccion);

        this.extremo.add(this.velocidadExtremo);
    }

    mostrar()
    {
        stroke(this.color);
        line(this.ancla.x, this.ancla.y, this.extremo.x, this.extremo.y);
    }
}
