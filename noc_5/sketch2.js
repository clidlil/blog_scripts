let color = [85, 142, 11];
let fondo = 220;
let rectangulo;
let rapidez = 0.005;

function setup()
{ 
    createCanvas(400, 400);
    let posicion = createVector(width/2, height/2);
    rectangulo = new Rectangulo(posicion, color, 15);
}

function draw()
{
    let atractor = createVector(mouseX, mouseY);
    let direccion = p5.Vector.sub(atractor, rectangulo.posicion);
    let fuerza = direccion.mult(rapidez);
    rectangulo.aplicarFuerza(fuerza);
    rectangulo.verificarBorde();
    rectangulo.mover();


    background(fondo);
    rectangulo.mostrar();
}
