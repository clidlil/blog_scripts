let angulo = 0;
let velocidadAngular = 0.05;

function setup()
{
    createCanvas(640, 360);
}

function draw()
{
    background(200);

    let amplitud = 300;
    let x = amplitud * cos(angulo);
    angulo += velocidadAngular;
    
    stroke(10);
    fill(150);
    translate(width/2, height/2);
    line(0, 0, x, 0);
    ellipse(x, 0, 20, 20);
}
