class Pendulo
{
    constructor(origen, radio, gravedad)
    {
        this.origen = origen.copy();
        this.posicion;
        this.radio = radio;
        this.angulo = PI/4;
        this.gravedad = gravedad;

        this.velocidadAngular = 0;
        this.aceleracionAngular = 0;
        this.friccion = 0.9;
    }

    actualizar()
    {
        this.aceleracionAngular = (-1 * this.gravedad / this.radio) *
            sin(this.angulo);

        this.velocidadAngular += this.aceleracionAngular;
        this.angulo += this.velocidadAngular;

        // this.velocidadAngular *= this.friccion;
    }

    mostrar()
    {
        this.posicion  = createVector(this.radio * sin(this.angulo),
            this.radio * cos(this.angulo));
        this.posicion.add(this.origen);

        stroke(0);
        line(this.origen.x, this.origen.y, this.posicion.x, this.posicion.y);
        fill(175);
        ellipse(this.posicion.x, this.posicion.y, 16, 16);
    }
}
