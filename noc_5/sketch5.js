let resorte;
let fuerza;
let gravedad;

function setup()
{
    createCanvas(500, 500);
    background(65);
    let ancla = createVector(width/2, 50);
    let extremo = createVector(width/2, height/2);
    let rigidez = 0.01;
    let color = [20, 200, 230];
    resorte = new Resorte(ancla, extremo, rigidez, color);
    gravedad = createVector(0, 0.4);
}

function draw()
{
    background(65);
    fuerza = p5.Vector.sub(resorte.extremo, resorte.ancla);
    resorte.deformar(fuerza);
    //resorte.deformar(gravedad);
    resorte.mostrar();
}

function mouseClicked()
{
    resorte.extremo = createVector(mouseX, mouseY);
}
