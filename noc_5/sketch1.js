let angspeed = 0.05;
let angle = 0.0;
function setup()
{ 
    createCanvas(400, 400);
    fill(1, 205, 110);
    strokeWeight(4);
    background(150);
    translate(width/2, height/2);
    line(0, -100, 0, 100);
    ellipse(0, -100, 30, 30);
    ellipse(2, 100, 30, 30);
 }

function draw()
{
    background(150);
    angle += angspeed;
    translate(width/2, height/2);
    rotate(angle);
    line(0, -100, 0, 100);
    ellipse(0, -100, 30, 30);
    ellipse(2, 100, 30, 30);
}
