// class Pelota
// {
//    constructor(posicion, color, diametro)
//    {
//       this.posicion = posicion;
//       this.color = color;
//       this.diametro = diametro;
//    }
// 
//    mover(velocidad)
//    {
//       this.posicion.add(velocidad);
//    }
// 
//    verificarBorde()
//    {
//       let radio = this.diametro/2;
//       if (this.posicion.x > width + radio)
//          this.posicion.x = -radio;
//       if (this.posicion.x < -radio)
//          this.posicion.x = width + radio;
// 
//       if (this.posicion.y > height + radio)
//          this.posicion.y = -radio;
//       if (this.posicion.y < -radio)
//          this.posicion.x = height + radio;
//    }
// 
//    mostrar()
//    {
//       stroke(35);
//       fill(this.color);
//       ellipse(this.posicion.x, this.posicion.y, this.diametro, this.diametro);
//    }
// }

class Pelota
{
   constructor(posicion, color, diametro)
   {
      this.posicion = posicion;
      this.color = color;
      this.diametro = diametro;
      this.velocidadMaxima = 8;
      this.velocidad = createVector(0, 0);
   }

   mover()
   {
      let raton = createVector(mouseX, mouseY);
      let aceleracion = p5.Vector.sub(raton, this.posicion);
      aceleracion.normalize();
      aceleracion.mult(0.5);

      this.velocidad.add(aceleracion);
      this.velocidad.limit(this.velocidadMaxima);
      this.posicion.add(this.velocidad);
   }

   verificarBorde()
   {
      let radio = this.diametro/2;
      if (this.posicion.x > width + radio)
         this.posicion.x = -radio;
      if (this.posicion.x < -radio)
         this.posicion.x = width + radio;

      if (this.posicion.y > height + radio)
         this.posicion.y = -radio;
      if (this.posicion.y < -radio)
         this.posicion.x = height + radio;
   }

   mostrar()
   {
      stroke(35);
      fill(this.color);
      ellipse(this.posicion.x, this.posicion.y, this.diametro, this.diametro);
   }
}
