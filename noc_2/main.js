let pelota;

let color = [85, 142, 11];
let fondo = 220;
let diametro = 45;

let aceleracion; 

function setup()
{ 
   createCanvas(400, 400);
   let posicion = createVector(width/2, height/2);
   pelota = new Pelota(posicion, color, diametro);
}

function draw()
{
   background(fondo);
   pelota.mostrar();
   pelota.mover();
   pelota.verificarBorde();
}
