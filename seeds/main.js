/* Spyral Seeds visulization. 
 * Geri Ochoa
 * Based on video by "Numberphile"
 * (https://www.youtube.com/watch?v=sj8Sg8qnjOg)
 */


let circles = 600;
let ratio = 1.0;
let rincrement = 0.000005;
let radius = 0.0;
let seedSize = 5;
var canvasSize = 400;

let canvasHolder;
let buttonPlay;
let buttonDirection;
let buttonPi;
let buttonRoot;
let buttonGolden;
let buttonSubmit;
let valueInput;

let rightDirection = true;
let playing = false;

function setup ()
{
   canvasHolder = document.getElementById("canvasHolder");
   createCanvas(canvasSize, canvasSize).parent(canvasHolder);
   textSize(16);

   let controls = document.getElementById("controls");

   buttonPlay = createButton(" > ");
   buttonPlay.parent(controls);
   buttonPlay.mousePressed(toggleAnimation);
   buttonDirection = createButton(" << ");
   buttonDirection.parent(controls);
   buttonDirection.mousePressed(toggleDirection);
   
   buttonPi = createButton(" Pi ");
   buttonPi.parent(controls);
   buttonRoot = createButton(" sqrt 2 ");
   buttonRoot.parent(controls);
   buttonGolden = createButton( " Phi ");
   buttonGolden.parent(controls);
   valueInput = createInput(1.05);
   valueInput.parent(controls);
   buttonSubmit = createButton( "submit");
   buttonSubmit.parent(controls);

   buttonPi.mousePressed(function() {startPosition(Math.PI)});
   buttonRoot.mousePressed(function() {startPosition(Math.sqrt(2))});
   buttonGolden.mousePressed(function() {startPosition(1.61803398874)});
   buttonSubmit.mousePressed(
      function()
      {
         let value = parseFloat(valueInput.value());
         if (value > 1 && value < 10)
         {
            startPosition(value);
         }
         else
         {
            alert("Invalid value");
         }
      });
}

function draw ()
{
   let ratioinc = 0.0;

   if (playing)
   {
      ratioinc = rincrement;
      if (!rightDirection)
      {
         ratioinc = -rincrement;
      }
   }


   background(20);

   ratio += ratioinc;
   let angleIncrement = (2*Math.PI)/ratio;

   noStroke;

   let angle = 0;
   radius = 2*seedSize;

   push();
   translate(width/2, height/2);

   for (let i = 1; i < circles; i++)
   {
      radius += 2*seedSize/(4 + i/5);
      g = map(radius, 0, width/2, 0, 255);
      fill(g, 200, 21);
      while (angle < 2*Math.PI)
      {
         position = p2c(radius, angle);
         ellipse(position.x, position.y, seedSize, seedSize);
         angle += angleIncrement;
      }
      angle -= 2*Math.PI;
   }
   pop();
   fill(200);
   text(ratio.toPrecision(7), 30, 20);
}

function startPosition(r)
{
   ratio = r;
   playing = true;
   rightDirection = false;
   toggleAnimation();
   toggleDirection();
}


function toggleAnimation()
{
   playing = !playing;
   if (playing)
   {
      buttonPlay.html(" || ");
   } 
   else
   {
      buttonPlay.html(" > ");
   }
}

function toggleDirection()
{
   rightDirection = !rightDirection;
   if (rightDirection)
   {
      buttonDirection.html(" << ");
   }
   else
   {
      buttonDirection.html(" >> ");
   }
}

function p2c(radius, angle)
{
   /* Convert polar coordinates into cartesian coordinates*/
   let posx = radius * Math.cos(angle);
   let posy = radius * Math.sin(angle);
   let position = new p5.Vector(posx, posy);
   return position;
}


