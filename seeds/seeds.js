let circles = 600;
let ratio = 1.0;
// Golden ratio
// let ratio = 1.61803398874;
let ratioinc = 0.000015;
// let ratioinc = 0.0;
let radius = 0.0;

// Golden Ratio 1.61803398874

let seedSize = 5;

function setup()
{
   createCanvas(400, 400);
}

function draw()
{
   background(120);

   ratio += ratioinc;
   let angleIncrement = (2*Math.PI)/ratio;

   fill(122, 14, 21);
   noStroke;

   let angle = 0;
   radius = 2*seedSize;

   push();
   translate(width/2, height/2);
   let semillas = 0;

   for (let i = 1; i < circles; i++)
   {
      radius += 2*seedSize/(4 + i/5);
      while (angle < 2*Math.PI)
      {
         position = p2c(radius, angle)
         ellipse(position.x, position.y, seedSize, seedSize);
         angle += angleIncrement;
         semillas++;
         // console.log(angle);
      }
      angle -= 2*PI;
   }
   pop();
}

function p2c(radius, angle)
{
   /* Convert polar coordiantes into cartesian coordinates)*/
   let posx = radius * Math.cos(angle);
   let posy = radius * Math.sin(angle);
   let position = new p5.Vector(posx, posy);
   return position;
}
