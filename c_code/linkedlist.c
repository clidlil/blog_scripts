#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct _nodo
{
    char *palabra;
    struct _nodo *siguiente;
} Nodo;

Nodo *crear_nodo(char *texto)
{
    Nodo *nodo = NULL;
    nodo = (Nodo *)malloc(sizeof(Nodo));
    nodo->palabra = texto;
    nodo->siguiente = NULL;
    return nodo;
}

void print_list(Nodo *lista)
{
    Nodo *temporal = NULL;
    temporal = lista;
    while (temporal)
    {
        printf("elemento: %s \n", temporal->palabra);
        temporal = temporal->siguiente;
    }
}

void insertar_elemento(Nodo **lista, Nodo *nuevo)
{
    Nodo **rastreador = lista;

    while ((*rastreador != NULL) && 
            strcmp((*rastreador)->palabra, nuevo->palabra) < 1)
    {
        rastreador = &(*rastreador)->siguiente;
    }
    nuevo->siguiente = *rastreador;
    *rastreador = nuevo;
}

int main(void)
{
    Nodo *cabeza = NULL;
    cabeza = crear_nodo("nariz");
    insertar_elemento(&cabeza, crear_nodo("papel"));
    insertar_elemento(&cabeza, crear_nodo("zapato"));
    insertar_elemento(&cabeza, crear_nodo("elefante"));
    insertar_elemento(&cabeza, crear_nodo("pie"));

    print_list(cabeza);

    return 0;
}
