class Liquido
{
    constructor(esquinas, coef_arrastre, color)
    {
        this.esquinas = esquinas;
        this.coef_arrastre = coef_arrastre;
        this.color = color;
        console.log(this.esquinas);
    }

    mostrar()
    {
        noStroke();
        fill(this.color);
        rect(this.esquinas.nw.x,
             this.esquinas.nw.y,
             this.esquinas.se.x,
             this.esquinas.se.y);
    }
}
