let pelotas = [];
let color = [85, 142, 11];
let fondo = 220;
let liquido;

function setup()
{ 
    createCanvas(400, 400);

    const color_liquido = [55, 100, 132, 200];
    const coef_arrastre = 0.2;
    const esquinas = 
        {
            nw: createVector(0, 2*height/3),
            se: createVector(width, height)
        }
    liquido = new Liquido(esquinas, coef_arrastre, color_liquido)



    for (let i = 0; i < 3; i++)
    {
        let masa = 20*i + 20;
        const pelota = new PelotaArrastre(
            createVector(85*i + 85, 100 - masa/2),
            color,
            masa
        ); 

        pelotas.push(pelota);
    }
}

function draw()
{
    background(fondo);
    let gravedad = createVector(0, 0.2);
    let viento = createVector(1.2, 0);

    const coefFriccion = 0.15;
    for (const pelota of pelotas)
    {
        let fuerzag = gravedad.copy();
        fuerzag.mult(pelota.masa);
  
        let friccion = pelota.velocidad.copy();
        friccion.normalize();
        friccion.mult(-1*coefFriccion);
        if (pelota.enLiquido(liquido))
        {
            pelota.aplicarArrastre(liquido);
        }
        pelota.aplicarFuerza(friccion);
        pelota.aplicarFuerza(viento);
        pelota.aplicarFuerza(fuerzag);
        pelota.mover();
        pelota.verificarBorde();
        pelota.mostrar();
    }
    liquido.mostrar();
}
