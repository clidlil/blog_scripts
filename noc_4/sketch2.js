let pelotas = [];
let color = [85, 142, 11];
let fondo = 220;

function setup()
{ 
    console.log("Test");
    createCanvas(400, 400);
    for (let i = 0; i < 3; i++)
    {
        let masa = 20*i + 20;
        const pelota = new Pelota(
            createVector(85*i + 85, 100 - masa/2),
            color,
            masa
        ); 

        pelotas.push(pelota);
    }
}

function draw()
{
    background(fondo);
    let gravedad = createVector(0, 0.2);
    let viento = createVector(1.2, 0);

    const coefFriccion = 0.15;
    for (const pelota of pelotas)
    {
        let fuerzag = gravedad.copy();
        fuerzag.mult(pelota.masa);

        let friccion = pelota.velocidad.copy();
        friccion.normalize();
        friccion.mult(-1*coefFriccion);
        pelota.aplicarFuerza(friccion);

        pelota.aplicarFuerza(viento);
        pelota.aplicarFuerza(fuerzag);
        pelota.mover();
        pelota.verificarBorde();
        pelota.mostrar();
    }
}
