class Node
{
   constructor(data)
   {
      this.data = data;
      this.next = null;
      this.previous = null;
   }
}

class LinkedList
{
   constructor(node)
   {
      this.head = node;
   }

   insertHead(node)
   {
      node.next = this.head;
      node.previous = null;
      this.head.previous = node;
      this.head = node;
   }

   insertAfter(newnode, predecessor)
   {
      let tmp = predecessor.next;
      if (tmp)
      {
         tmp.previous = newnode;
      }
      predecessor.next = newnode;
      newnode.previous = predecessor;
      newnode.next = tmp;
   }

   deleteNode(node)
   {
      if (!node.previous)
      {
         this.head = node.next;
      }
      else if (!node.next)
      {
         node.previous = null;
      }
      else
      {
         node.previous.next = node.next;
         node.next.previous = node.previous;
      }
   }
}
