let sistemaParticulas;
let fuerza;
let viento;

function setup()
{
   let canvas = createCanvas(400, 400);
   canvas.parent("canvasHolder1");
   let pos = createVector(width/2, height - 50);
   sistemaParticulas = new SistemaParticulas(pos);
   fuerza = createVector(0, -0.05);
   viento = createVector(-0.02, 0.02);
}

function draw()
{
   background(255);
   sistemaParticulas.agregarParticula();
   sistemaParticulas.aplicarFuerza(fuerza);
   sistemaParticulas.aplicarFuerza(viento);
   sistemaParticulas.correr();
}
