class Particula
{
   constructor(posicion)
   {
      this.posicion = posicion.copy();
      let vx = randomGaussian() * 0.3;
      let vy = randomGaussian() * 0.3 - 1;
      this.velocidad = createVector(vx, vy);
      this.aceleracion = createVector(0, 0);
      this.tiempoDeVida = 255;
   }

   aplicarFuerza(fuerza)
   {
      this.aceleracion.add(fuerza);
   }

   actualizar()
   {
      this.velocidad.add(this.aceleracion);
      this.posicion.add(this.velocidad);
      this.tiempoDeVida -= 2;
      this.aceleracion.mult(0);
   }

   estaMuerta()
   {
      return (this.tiempoDeVida <= 0);
   }

   mostrar()
   {
      let alpha = map(0, 1, 0, 255, this.tiempoDeVida);
      stroke(0, alpha);
      fill(175, alpha);
      ellipse(this.posicion.x, this.posicion.y, 8, 8);
   }
}

class SistemaParticulas
{
   constructor(pos)
   {
      this.posicion = pos.copy();
      let particula = new Particula(this.posicion);
      this.particulas = new LinkedList(particula);
   }

   agregarParticula()
   {
      let tparticula = new Particula(this.posicion);
      this.particulas.insertHead(tparticula);
   }

   aplicarFuerza(fuerza)
   {
      let particula = this.particulas.head;
      do
      {
         particula.aplicarFuerza(fuerza);
         particula = particula.next;
      } while(particula);
   }

   correr()
   {
      let particula = this.particulas.head;
      do
      {
         particula.actualizar();
         particula.mostrar();
         if (particula.estaMuerta())
         {
            this.particulas.deleteNode(particula);
         }
         particula = particula.next;
      } while(particula);
   }
}
