let spriteGrid = [];
let backgroundColor = [80, 89, 87];
let color = [210, 220, 205];
let canvasSize = 640;
let spriteSize = 16;
let squareSize = canvasSize / spriteSize;
let button;
let hexCode;

function setup()
{ 
    createCanvas(canvasSize, canvasSize);

    for (let y = 0; y < spriteSize; y++)
    {
        for (let x = 0; x < spriteSize; x++)
        {
            let xcord = x*squareSize;
            let ycord = y*squareSize;
            let pos = createVector(xcord, ycord);
            let rowcol = [x, y];

            let square = new Square(pos, backgroundColor, squareSize, rowcol, color);

            spriteGrid.push(square);
        }
    }

    button = createButton('Bin2Hex');
    button.mousePressed(printHexCode);
}

function draw()
{
    background(backgroundColor);
    for (let i = 0; i < spriteSize*spriteSize; i++)
    {
        spriteGrid[i].display();
    }
}

function mouseClicked()
{
    if (mouseX > 0 && mouseX < width && mouseY > 0 && mouseY < height)
    {
        let xpos = floor(mouseX / squareSize);
        let ypos = floor(mouseY / squareSize);

        let index = xpos + spriteSize * ypos;
        spriteGrid[index].switchState();
    }
}

function printHexCode()
{
    const hexLength = 4;

    if (!hexCode) {
        hexCode = createDiv("");
    }

    hexCode.html("{");

    let hexWords = computeHexCodes(spriteGrid, spriteSize);
    let spriteLine = "0x";
    
    for (let index = 0; index < hexWords.length; index++)
    {
        spriteLine += hexWords[index];
        if (!((index + 1) % (spriteSize/hexLength)))
        {
            if (index !== hexWords.length - 1)
            {
                spriteLine += ", ";
            }
            hexCode.html(spriteLine, true);
            spriteLine = "0x";
        }


    }
    hexCode.html("}", true);
}

function computeHexCodes(spriteGrid, spriteSize)
{
    const hexLength = 4;
    let bitMapper = 
    {
        "0000" : "0", 
        "1000" : "8",
        "0100" : "4",
        "1100" : "C",
        "0010" : "2",
        "1010" : "A",
        "0110" : "6",
        "1110" : "E",
        "0001" : "1",
        "1001" : "9",
        "0101" : "5",
        "1101" : "D",
        "0011" : "3",
        "1011" : "B",
        "0111" : "7",
        "1111" : "F",
    }

    let word="";
    let hexDigits=[];
    for (let index = 0; index < spriteGrid.length; index++)
    {
        let binDigit = "0";
        if (spriteGrid[index].isOn)
        {
            binDigit = "1";
        }
        word += binDigit;
        if (!((index + 1) % hexLength))
        {
            hexDigits.push(bitMapper[word]);
            word = "";
        }
    }
    return hexDigits;
}
