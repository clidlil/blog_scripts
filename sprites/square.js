class Square
{
    constructor(position, color, size, rowcol, bgcolor = 50)
    {
        this.position = position;
        this.color = color;
        this.size = size;
        this.isOn = false;
        this.bgcolor = bgcolor;
        this.rowcol = rowcol;
    }

    display()
    {
        if (this.isOn)
        {
            fill(this.color);
        }
        else
        {
            fill(this.bgcolor);
        }

        rect(this.position.x, this.position.y, this.size, this.size);
    }

    switchState()
    {
        this.isOn = !this.isOn;
    }
}
