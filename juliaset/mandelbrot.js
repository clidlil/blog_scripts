function Mandelbrot(p, micoords, macoords) {
   this.mincoords = p.createVector(micoords[0], micoords[1]);
   this.maxcoords = p.createVector(macoords[0], macoords[1]);
   this.maxiterations = 50;
   this.infinity = 20;
   
   this.show = function() {
      for (var x = 0; x < p.width; x++) {
         for (var y = 0; y < p.height; y++) {

            var c = [];
            var realpart = p.map(x, 0, p.width, this.mincoords.x, this.maxcoords.x);
            var imgpart = -p.map(y, 0, p.height, this.mincoords.y, this.maxcoords.y);
            c = [realpart, imgpart];

            iteration = 0;

            while (iteration < this.maxiterations) {
               var f = []; 
               f[0] = realpart**2 - imgpart**2;
               f[1] = 2 * realpart * imgpart;

               realpart = f[0] + c[0];
               imgpart = f[1] + c[1];

               if ((p.abs(realpart) + p.abs(imgpart)) > this.infinity) {
                  break;
               }

               iteration++;
            }

            var bri = 100;
            var hue = p.map(iteration, 0, this.maxiterations, 0, 1);
            hue = p.map(p.sqrt(hue), 0, 1, 100, 300);

            if (iteration === this.maxiterations) {
               bri = 0;
            }

            var sat = 50;
            col = p.color(hue, sat, bri);

            p.set(x, y, col);

         }
      }
      p.updatePixels();
   }
}
