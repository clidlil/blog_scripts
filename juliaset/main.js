/* Julia Sets visulization from points on the Mandelbrot set. 
 * Geri Ochoa
 * Based on a tutorial by "The Coding Train"
 * (https://www.youtube.com/watch?v=fAsaSkmbF5s)
 */

var sk1X;
var jconstant = [0, 0];
var mincoords = [-2, -2];
var maxcoords = [2, 2];
var canvasSize = 320;

var sketch1 = function (p) {
   var canvasHolder1;
   var mandelbrot;

   p.setup = function () {
      canvasHolder1 = document.getElementById("canvas1");
      p.createCanvas(canvasSize, canvasSize).parent(canvasHolder1);
	   p.colorMode(p.HSB, 360, 100, 100);

      p.pixelDensity(1);
      p.loadPixels();

      mandelbrot = new Mandelbrot(p, mincoords, maxcoords);
      mandelbrot.show();

      p.textSize(14);

      p.stroke(0, 100, 100, 0.3);
      p.line(0, p.height/2, p.width, p.height/2);
      p.line(p.width/2, 0, p.width/2, p.height);
      p.noStroke();
   };

   p.draw = function () {
      var x = 0;
      var y = 0;
      var coordTxt;


      if (p.mouseX > 0 && p.mouseX < p.width &&
         p.mouseY > 0 && p.mouseY < p.height){
            x = p.mouseX;
            y = p.mouseY;
      }

      jconstant[0] = p.map(x, 0, p.width, mincoords[0], maxcoords[0]);
      jconstant[1] = -p.map(y, 0, p.height, mincoords[1], maxcoords[1]);



      coordTxt = "Punto Actual: (" + jconstant[0].toFixed(4) + ", " + jconstant[1].toFixed(4) + "i)";

      p.rect(canvasSize - 250, canvasSize - 30, 235, 30)
      p.text(coordTxt, canvasSize - 240, canvasSize - 10);
   };
}


var sketch2 = function (p) {
   var canvasHolder2;
   var julia;

   p.setup = function () {
      canvasHolder2 = document.getElementById("canvas2");
      p.createCanvas(canvasSize, canvasSize).parent(canvasHolder2);
	   p.colorMode(p.HSB, 360, 100, 100);

      p.pixelDensity(1);
      p.loadPixels();

      julia = new Julia(p, mincoords, maxcoords);
   };

   p.draw = function () {
      julia.show(jconstant);

      p.updatePixels();
   };
}

fig1 = new p5(sketch1);

fig2 = new p5(sketch2);
