function Julia(p, micoords, macoords ) {
   this.mincoords = micoords;
   this.maxcoords = macoords;
   this.maxiterations = 20;
   this.infinity = 5;
   this.c = [0, 0];
   
   this.show = function(jconstant) {
      this.c = jconstant;
      for (var x = 0; x < p.width; x++) {
         for (var y = 0; y < p.height; y++) {

            var realpart = p.map(x, 0, p.width, this.mincoords[0], 
               this.maxcoords[0]);
            var imgpart = -p.map(y, 0, p.height, this.mincoords[1], 
               this.maxcoords[1]);
            var iteration = 0;

            while (iteration < this.maxiterations) {
               var f = []; 
               f[0] = realpart**2 - imgpart**2;
               f[1] = 2 * realpart * imgpart;

               realpart = f[0] + this.c[0];
               imgpart = f[1] + this.c[1];

               if ((p.abs(realpart) + p.abs(imgpart)) > this.infinity) {
                  break;
               }

               iteration++;
            }

            var bri = 100;
            //var sat = 50;
            var hue = p.sqrt(iteration / this.maxiterations);
            var sat = p.sqrt(iteration / this.maxiterations);
            hue = p.map(hue, 0, 1, 150, 360);
            sat = p.map(sat, 0, 1, 50, 100);


            if (iteration === this.maxiterations) {
               bri = 0;
            }

            var col = p.color(hue, sat, bri);

            p.set(x, y, col);
         }
      }
   }
}
