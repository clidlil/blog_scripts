function setup()
{
   createCanvas(400, 400);
   background(50);
   fill(220);

   let side = 20;
   let apothem = side*Math.sqrt(3)/2;
   let hdistance = 3*side;

   let horResolution = Math.floor(width/hdistance);
   let verResolution = Math.floor(height/apothem);
   
   let x;
   let y;

   for (let j = 0; j < verResolution; j++)
   {
      for (let i = 0; i < horResolution + 1; i++)
      {

         let offset = (j%2)*(3*side/2);

         x = side + i*hdistance + offset;
         y = apothem + j*apothem;
         hexagon(x, y, side);
      }
   }
}

function draw()
{
   
}

function hexagon(hx, hy, side)
{
   let x;
   let y;
   let angle;

   beginShape() 
   for (let counter = 0; counter < 6; counter++)
   {
      angle = counter*PI/3;
      x = hx + side*cos(angle);
      y = hy + side*sin(angle);
      vertex(x, y);
   }
   endShape(CLOSE);
}
