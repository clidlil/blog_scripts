let pelotas = [];
let color = [85, 142, 11];
let fondo = 220;

function setup()
{ 
    createCanvas(400, 400);
    for (let i = 0; i < 3; i++)
    {
        let masa = 25*i + 25;
        const pelota = new Pelota(
            createVector(80*i + 80, height/2 - masa/2),
            color,
            masa
        ); 

        pelotas.push(pelota);
    }
}

function draw()
{
    background(fondo);
    let gravedad = createVector(0, 0.2);
    for (const pelota of pelotas)
    {
        let fuerzag = gravedad.copy();
        fuerzag.mult(pelota.masa);
        pelota.aplicarFuerza(fuerzag);
        pelota.mover();
        pelota.verificarBorde();
        pelota.mostrar();
    }
}
