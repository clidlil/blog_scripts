class Pelota
{
   constructor(posicion, color, masa)
   {
      this.posicion = posicion;
      this.velocidad = createVector(0, 0);
      this.velocidadMaxima = 15;
      this.aceleracion = createVector(0, 0);
      this.masa = masa;

      this.color = color;
      this.diametro = masa;
   }

   aplicarFuerza(f)
   {
      let fuerza = f.copy();
      fuerza.div(this.masa);
      this.aceleracion.add(fuerza);
   }

   mover()
   {
      this.velocidad.add(this.aceleracion);
      this.velocidad.limit(this.velocidadMaxima);
      this.posicion.add(this.velocidad);

      this.aceleracion.mult(0);
   }

   verificarBorde()
   {
      let radio = this.diametro/2;
      if (this.posicion.x > width - radio)
      {
         this.posicion = width - radio;
         this.velocidad.x *= -1;
      }
      if (this.posicion.x < radio)
      {
         this.posicion.x = radio;
         this.velocidad.x *= -1;
      }

      if (this.posicion.y > height - radio)
      {
         this.posicion.y = height - radio;
         this.velocidad.y *= -1;
      }
      if (this.posicion.y < radio)
      {
         this.posicion.y = radio;
         this.velocidad.y *= -1;
      }
   }

   mostrar()
   {
      stroke(35);
      fill(this.color);
      ellipse(this.posicion.x, this.posicion.y, this.diametro, this.diametro);
   }
}
