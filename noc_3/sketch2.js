let pelotas = [];
let color = [85, 142, 11];
let fondo = 220;

function setup()
{ 
    createCanvas(400, 400);
    for (let i = 0; i < 3; i++)
    {
        let masa = 25*i + 25;
        const pelota = new Pelota(
            createVector(80*i + 80, height/2 - masa/2),
            color,
            masa 
        ); 

        pelotas.push(pelota);
    }
}

function draw()
{
    background(fondo);
    let fuerza = createVector(0, 1.3);
    for (const pelota of pelotas)
    {
        if (mouseIsPressed)
           pelota.aplicarFuerza(fuerza); 
        
        pelota.mover();
        pelota.verificarBorde();
        pelota.mostrar();
    }
}
