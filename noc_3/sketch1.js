let pelota;

let color = [85, 142, 11];
let fondo = 220;
let masa = 20;

function setup()
{ 
   createCanvas(400, 400);
   let posicion = createVector(width/2, height/2);
   pelota = new Pelota(posicion, color, masa);
}

function draw()
{
   background(fondo);
   let fuerza = createVector(0, 1.3);
   if (mouseIsPressed)
   {  
      pelota.aplicarFuerza(fuerza);
   }
   pelota.mover();
   pelota.verificarBorde();
   pelota.mostrar();
}
